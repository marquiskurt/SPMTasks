// The Swift Programming Language
// https://docs.swift.org/swift-book

import Foundation

func greet(person: String) {
    print("Hey there, \(person)!")
}

let greeters = CommandLine.arguments[1...]

for greetee in greeters {
    greet(person: greetee)
}
