### Version 1.1.1

#### Changed

- Updated repository and bug reports URLs to the new Swift Dev Collective
  organization.

## Version 1.1

#### Added

- Output and formatting can now be configured per-task in addition to the
  project level.

### Version 1.0.1

#### Fixed

- Swift Build task would fail because `data.type` was undefined.

## Version 1.0

Initial release
