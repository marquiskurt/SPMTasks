module.exports = {
  getFormatterInformation,
};

function getFormatterInformation(config) {
  let useSharedSettings = config.get("use-shared-format-settings", "boolean");

  let projectFormatted = config.get("package-prettify-output");
  let projectFormattingBinary = config.get("package-prettify-command");
  let projectFormattingBinaryPath = config.get("package-prettify-command-path");

  let workspaceFormatted = nova.workspace.config.get(
    "spm-tasks.prettify-output"
  );
  let workspaceFormattingBinary = nova.workspace.config.get(
    "spm-tasks.prettify-command"
  );
  let workspaceFormattingBinaryPath = nova.workspace.config.get(
    "spm-tasks.prettify-command-path"
  );

  if (useSharedSettings) {
    return deriveFormatterCommandArguments({
      enabled: workspaceFormatted,
      binary: workspaceFormattingBinary,
      binaryPath: workspaceFormattingBinaryPath,
    });
  }

  return deriveFormatterCommandArguments({
    enabled: projectFormatted,
    binary: projectFormattingBinary,
    binaryPath: projectFormattingBinaryPath,
  });
}

function deriveFormatterCommandArguments(formatting) {
  if (!formatting.enabled || !formatting.binary) {
    return [];
  }

  if (formatting.binary == "custom" && formatting.binaryPath) {
    return ["|", formatting.binaryPath];
  }
  return ["|", formatting.binary];
}
