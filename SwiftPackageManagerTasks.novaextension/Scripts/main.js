const { getFormatterInformation } = require("./formatting.js");
const { getCommonPackageData } = require("./sharedpackage.js");

let taskProvider = null;

exports.activate = function () {
  taskProvider = new SPMTaskProvider();
  nova.assistants.registerTaskAssistant(taskProvider, {
    identifier: "spm-tasks",
    name: "Swift Package Manager",
  });
};

exports.deactivate = function () {
  taskProvider = null;
};

class SPMTaskProvider {
  constructor() {}

  resolveTaskAction(context) {
    let action = context.action;
    let data = context.data;
    let config = context.config;

    if (action == Task.Build) {
      return this.resolveBuildTaskAction(config, data);
    }

    if (action == Task.Run && data.type == "run") {
      return this.resolveRunTaskAction(config);
    }

    if (action == Task.Run && data.type == "test") {
      return this.resolveTestTaskAction(config);
    }
    return null;
  }

  resolveBuildTaskAction(config, data) {
    let commonPackageConfig = getCommonPackageData(config);
    let commandArguments = ["build", ...commonPackageConfig];

    if (data != undefined && data.type == "test") {
      commandArguments.push("--build-tests");
    }

    return new TaskProcessAction(getToolchainCommand(), {
      args: [...commandArguments, ...getFormatterInformation(config)],
      shell: true,
    });
  }

  resolveRunTaskAction(config) {
    let executableName = config.get("package-executable-name", "string");
    let argumentsAtLaunch = config.get("package-launchargs", "stringArray");
    let commonPackageConfig = getCommonPackageData(config);
    let commandArguments = ["run", "--skip-build", ...commonPackageConfig];

    if (executableName) {
      commandArguments.push(executableName);
    }

    if (argumentsAtLaunch) {
      commandArguments = [...commandArguments, ...argumentsAtLaunch];
    }

    return new TaskProcessAction(getToolchainCommand(), {
      args: commandArguments,
      shell: true,
    });
  }

  resolveTestTaskAction(config) {
    let getCodeCoverage = config.get("package-code-coverage", "boolean");
    let enableParallel = config.get(
      "package-enable-parallelization",
      "boolean"
    );
    let parallelWorkers = config.get("package-num-workers", "number");
    let filterTests = config.get("package-test-filter", "string");
    let skipTests = config.get("package-test-skip", "string");
    let commonPackageConfig = getCommonPackageData(config);
    let commandArguments = ["test", "--skip-build", ...commonPackageConfig];

    if (getCodeCoverage) {
      commandArguments.push("--enable-code-coverage");
    }

    if (enableParallel) {
      commandArguments.push("--parallel");

      if (parallelWorkers) {
        commandArguments.push("--num-workers");
        commandArguments.push(`${parallelWorkers}`);
      }
    }

    if (filterTests) {
      commandArguments.push("--filter");
      commandArguments.push(filterTests);
    }

    if (skipTests) {
      commandArguments.push("--skip");
      commandArguments.push(skipTests);
    }

    return new TaskProcessAction(getToolchainCommand(), {
      args: [...commandArguments, ...getFormatterInformation(config)],
      shell: true,
    });
  }
}

/// Retrieves the toolchain to execute.
function getToolchainCommand() {
  let toolchain = nova.config.get("spm-tasks.toolchain");
  let toolchainPath = nova.config.get("spm-tasks.toolchain-path");

  if (toolchain == "custom") {
    return toolchainPath;
  }

  return "swift";
}
