module.exports = {
  getCommonPackageData,
};

/** Retrieves common package data, such as sandbox settings, package paths, and other configurations. */
function getCommonPackageData(config) {
  let useSharedSettings = config.get("use-shared-settings", "boolean");

  let packagePath = config.get("package-path", "string");
  let packageConfig = config.get("package-config", "string");
  let disableSandbox = config.get("package-disable-sandbox", "boolean");

  let sharedPackagePath = nova.workspace.config.get(
    "spm-tasks.shared-package-path",
    "string"
  );
  let sharedPackageConfig = nova.workspace.config.get(
    "spm-tasks.shared-package-config",
    "string"
  );
  let sharedDisableSandbox = nova.workspace.config.get(
    "spm-tasks.shared-package-disable-sandbox",
    "boolean"
  );

  let sharedCachePath = nova.workspace.config.get(
    "spm-tasks.shared-package-cache",
    "string"
  );
  let sharedScratchPath = nova.workspace.config.get(
    "spm-tasks.shared-package-scratch",
    "string"
  );

  let packageArguments = [];

  if (useSharedSettings && sharedPackagePath) {
    packageArguments.push("--package-path");
    packageArguments.push(sharedPackagePath);
  } else if (packagePath) {
    packageArguments.push("--package-path");
    packageArguments.push(packagePath);
  }

  if (useSharedSettings && sharedPackageConfig) {
    packageArguments.push("--configuration");
    packageArguments.push(sharedPackageConfig);
  } else if (packageConfig) {
    packageArguments.push("--configuration");
    packageArguments.push(packageConfig);
  }

  if (useSharedSettings && sharedDisableSandbox) {
    packageArguments.push("--disable-sandbox");
  } else if (disableSandbox) {
    packageArguments.push("--disable-sandbox");
  }

  if (sharedScratchPath) {
    packageArguments.push("--scratch-path");
    packageArguments.push(sharedScratchPath);
  }

  if (sharedCachePath) {
    packageArguments.push("--cache-path");
    packageArguments.push(sharedCachePath);
  }

  return packageArguments;
}
