<!--
👋 Hello! As Nova users browse the extensions library, a good README can help them understand what your extension does, how it works, and what setup or configuration it may require.

Not every extension will need every item described below. Use your best judgement when deciding which parts to keep to provide the best experience for your new users.

💡 Quick Tip! As you edit this README template, you can preview your changes by selecting **Extensions → Activate Project as Extension**, opening the Extension Library, and selecting "{{ name }}" in the sidebar.

Let's get started!
-->

<!--
🎈 Include a brief description of the features your extension provides. For example:
-->

**Swift Tasks** provides Task integration with the Swift Package Manager, allowing developers to build, run, and test their Swift packages easily. It works well with Panic's [Icarus](https://extensions.panic.com/extensions/panic/panic.Icarus/) extension for Swift by providing the tasks to run Swift code.

<!--
🎈 It can also be helpful to include a screenshot or GIF showing your extension in action:
-->

![A screenshot of Nova with the Swift Tasks extension enabled](https://gitlab.com/SwiftDevCollective/SPMTasks/-/raw/root/SwiftPackageManagerTasks.novaextension/Images/screenshot.png)

## Requirements

<!--
🎈 If your extension depends on external processes or tools that users will need to have, it's helpful to list those and provide links to their installers:
-->

Swift Tasks requires a version of the Swift toolchain installed, either through Xcode itself, the Xcode Command Line tools, or the toolchain available at https://swift.org.

In the extension's settings, you can configure which toolchain to use.

## Usage

<!--
🎈 If users will interact with your extension manually, describe those options:
-->

Swift Tasks offers three different tasks for Swift packages:

- **Swift Build** builds the package.
- **Swift Run** builds the package and runs a specified executable. This is ideal for command line tools written in a Swift package.
- **Swift Test** builds the package for testing and runs the package's unit tests.

### Configuration

<!--
🎈 If your extension offers global- or workspace-scoped preferences, consider pointing users toward those settings. For example:
-->

To configure global preferences, open **Extensions → Extension Library...** then select Swift Tasks's **Preferences** tab.

You can also configure preferences on a per-project basis in **Project → Project Settings...**

Finally, each task provides its own settings which you can configure.

---

The Swift logo is a trademark of Apple Inc.